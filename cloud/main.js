 /* global select */
var _ = require('underscore');

var Championship = Parse.Object.extend('Campeonato');
var Match = Parse.Object.extend('Partida');
var RealTimeMatch = Parse.Object.extend('PartidaEmAndamento');
var Score = Parse.Object.extend('Classificacao');
var GoalScores = Parse.Object.extend('Artilharia');
var PlayByPlay = Parse.Object.extend('Narracao');
var Team = Parse.Object.extend('Equipe');
var LineUp = Parse.Object.extend('Escalacao');

var asf = require('cloud/helpers/after_save_functions.js');
var ph = require('cloud/helpers/push_helper.js');

var cps = require('cloud/controllers/championships.js');
var gcs = require('cloud/controllers/goal_scorers.js');
var mcs = require('cloud/controllers/matches.js');
var scs = require('cloud/controllers/scores.js');
var pbp = require('cloud/controllers/playbyplay.js');
var tms = require('cloud/controllers/teams.js');
var lns = require('cloud/controllers/lineup.js');
var mst = require('cloud/controllers/matchStatistics.js');
var tst = require('cloud/controllers/teamStatistics.js');


Parse.Cloud.job("CrowlerChamps", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    console.log('-------------------------------------------CrowlerChamps ------------------------');

    cps.championships(request).then(function(obj){
        status.success("CrowlerChamps completed successfully.");
    }, function(error) {
        status.error("CrowlerChamps, error " + JSON.stringify(error));
    });

});

Parse.Cloud.job("CrowlerTeams", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Championship);
    query.select("Id", "Finalizado");
    query.each(function(result) {
        if(!result.get("Finalizado")){
            return tms.teams(request,result.get("Id"));
        } else {
            return result;
        }
    }).then(function(){
        status.success("CrowlerTeams completed successfully.");
    }, function(error) {
        status.error("CrowlerTeams, error " + JSON.stringify(error));
    });

});

Parse.Cloud.job("CrowlerGoalScores", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Championship);
    query.select("Id", "Finalizado");
    query.each(function(result) {
        if(!result.get("Finalizado")){
            return gcs.goalScorers(request, result.get("Id"));
        } else {
            return result;
        }
    }).then(function(){
        status.success("CrowlerGoalScores completed successfully.");
    }, function(error) {
        status.error("CrowlerGoalScores, error " + JSON.stringify(error));
    });
});


Parse.Cloud.job("CrowlerMatches", function(request, status) {
    console.log('******************************************');
    console.log('CrowlerMatches >>> Start updating');

    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Championship);
    query.select("Id", "Finalizado");

    query.each(function(result) {
        console.log('>>> CrowlerMatches: =>' + result.get("Id") + " Finalizado? " + result.get("Finalizado"));
        if(!result.get("Finalizado")){
            return mcs.matches(request,result.get("Id"));
        }
        return result;
    }).then(function(results){
        console.log('CrowlerMatches >>> Start updating lineups');
        return lns.updateLineUpsPartidasAtuais(request);
     }).then(function(){
        status.success("CrowlerMatches >>>  completed successfully.");
        console.log('CrowlerMatches >>> End updating');
        console.log('******************************************');
    }, function(error) {
        status.error("CrowlerMatches >>> , error " + JSON.stringify(error));
        console.log('CrowlerMatches >>> End updating');
        console.log('******************************************');
    });
});

Parse.Cloud.job("CrowlerUpdateLast2DaysPlayByPlay", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Match);
    query.select('Finalizado',
                 'Id', 'Data',
                 'NomeMandante', 'SearchNameMandante',
                 'NomeVisitante', 'SearchNameVisitante');
    var beforeYesterdayInBrazil = new Date();
    beforeYesterdayInBrazil.setHours(beforeYesterdayInBrazil.getHours() - (3 + 240));
    
    query.each(function(result){
        if(result.get("Finalizado") && result.get('Data') >= beforeYesterdayInBrazil){
            return pbp.updatePlayByPlay(request, result.get("Id"), result.get("Data"),
                                        result.get("NomeMandante"), result.get("NomeVisitante"),
                                        result.get("SearchNameMandante"), result.get("SearchNameVisitante"));
        }
        
        return result;
    }).then(function(){
        status.success("CrowlerUpdateLast2DaysPlayByPlay completed successfully.");
    }, function(error) {
        status.error("CrowlerUpdateLast2DaysPlayByPlay, error " + JSON.stringify(error));
    });
});

Parse.Cloud.job("CrowlerPlayByPlay", function(request, status) {
    // Set up to modify user data   
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('Narracao >>> Start updating play-by-plays');
    
    var successMessage = "CrowlerPlayByPlay completed successfully.";
    var query = new Parse.Query(Match);
    query.select('PodePegarNarracoes',
                 'Id', 'Data',
                 'NomeMandante', 'SearchNameMandante',
                 'NomeVisitante', 'SearchNameVisitante');
    query.equalTo("PodePegarNarracoes", true);
    return query.find(function(results) {
        if (results != null) {
            console.log('Narracao >>> Total de registros: ' + results.length);
        } else {
            console.log('Narracao >>> Total de registros: 0');
        }
        
        return results;
    }).then(function(results) {
        new_results = []
        _.each(results, function(result) {
            console.log('Narracao >>> Id Partida: ' + result.get("Id"));
            if(result.get("PodePegarNarracoes")){
                new_results.push(pbp.updatePlayByPlay(request, result.get("Id"), result.get("Data"),
                                            result.get("NomeMandante"), result.get("NomeVisitante"),
                                            result.get("SearchNameMandante"), result.get("SearchNameVisitante")));
            }
        })
        console.log('Narracao >>> Total de registros new_results: ' + new_results.length);
        return Parse.Promise.when(new_results);
     }).then(function(results){
        console.log('Narracao >>> Start updating current matches');
        return mcs.updatePartidasAtuais(request);
     }).then(function(){
        console.log('Narracao >>> Start updating lineUps');
        return lns.updateAllLineUpsBasedOnPlayByPlay(request);
     }).then(function(){
        console.log('Narracao >>> Start sending push notifications');
        return pbp.sendPushNotification(request);
     }).then(function(results){
        console.log('Narracao >>> Start updating GoalScores');
        var queryGoalScores = new Parse.Query(Championship);
        queryGoalScores.select("Id", "Finalizado");
        return queryGoalScores.each(function(goalScore) {
            if(!goalScore.get("Finalizado")){
                return gcs.goalScorers(request, goalScore.get("Id"));
            } else {
                return goalScore;
            }
        });
  //   }).then(function(results){
  //      console.log('Start updating MatchStatistics');
  //      return mst.updateMatchStatistics(request);
  //   }).then(function(results){
  //      console.log('Start updating TeamStatistics');
  //      return tst.updateTeamStatistics(request);
    }).then(function() {
        console.log('******* Narracao >>> End updating play-by-plays');
        console.log('******************************************');
    }).then(function() {
        status.success(successMessage);
     }, function(error) {
        status.error("CrowlerPlayByPlay, error " + JSON.stringify(error));
     });
});

Parse.Cloud.job("CrowlerLineupsThaHasBegan", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    lns.crowlLineUpsThaHasBegan(request, status).then(function(){
        status.success("CrowlerLineupsThaHasBegan completed successfully.");
    }, function(error) {
        status.error("CrowlerLineupsThaHasBegan, error " + JSON.stringify(error));
    });
});

Parse.Cloud.job("CrowlerLineupsUnfinished", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    lns.crowlLineUpsUnfinished(request, status).then(function(){
        status.success("CrowlerLineupsUnfinished completed successfully.");
    }, function(error) {
        status.error("CrowlerLineupsUnfinished, error " + JSON.stringify(error));
    });
});

Parse.Cloud.job("CrowlerScores", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    
    var query = new Parse.Query(Championship);
    query.select("Id", "Finalizado", "TemClassificacao");

    return query.find(function(results) {
        if (results != null) {
            console.log('CrowlerScores >>> Total de registros: ' + results.length);
        } else {
            console.log('CrowlerScores >>> Total de registros: 0');
        }
        
        return results;
    }).then(function(results) {
        var promises = [];
        _.each(results, function(result) {           
            if(!result.get("Finalizado") && result.get("TemClassificacao")){
                promises.push(scs.scores(request, result.get("Id")));
                console.log('CrowlerScores >>> Id Campeonato: ' + result.get("Id"));
            }
        })
        console.log('CrowlerScores >>> Total de registros promises: ' + promises.length);
        return Parse.Promise.when(promises);
    }).then(function(){
        status.success("CrowlerScores completed successfully.");
    }, function(error) {
        status.error("CrowlerScores, error " + JSON.stringify(error));
    });

});


Parse.Cloud.job("CrowlerMatchStatistics", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey()
    
    return mst.updateMatchStatistics(request).then(function(results){
        console.log('triggering CrowlerTeamStatistics Job')
        return tst.callmeAsAJob();
    }).then(function(){
        status.success("CrowlerMatchStatistics completed successfully.");
    }, function(error) {
        status.error("CrowlerMatchStatistics, error " + JSON.stringify(error));
    });

});

Parse.Cloud.job("CrowlerTeamStatistics", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey()
    
    return tst.updateTeamStatistics(request).then(function(){
        status.success("CrowlerTeamStatistics completed successfully.");
    }, function(error) {
        status.error("CrowlerTeamStatistics, error " + JSON.stringify(error));
    });

});

Parse.Cloud.job("cleanAllDuplicatedEntries", function(request, status) {
    Parse.Cloud.useMasterKey();
    
    console.log("Start cleaning all duplicated entries");
    
    console.log("*********** Start cleaning play-by-play");
    asf.queryAllFromClassAndRemoveDuplicates(PlayByPlay,["Id"]).then(function(){
        console.log("Start cleaning lineups");
        return asf.queryAllFromClassAndRemoveDuplicates(LineUp,["IdMandante","IdVisitante","IdPartida"]);
    }).then(function(){
        console.log("Start cleaning scores");
        return asf.queryAllFromClassAndRemoveDuplicates(Score,["IdEquipe","IdCampeonato"]);
    }).then(function(){
        console.log("Start cleaning championships");
        return asf.queryAllFromClassAndRemoveDuplicates(Championship,["Id"]);
    }).then(function(){
        console.log("Start cleaning teams");
       return asf.queryAllFromClassAndRemoveDuplicates(Team,["Id"]);
    }).then(function(){
        console.log("Start cleaning goal scores");
       return asf.queryAllFromClassAndRemoveDuplicates(GoalScores,["IdJogador"]);
    }).then(function(){
        console.log("Start cleaning matches");
       return asf.queryAllFromClassAndRemoveDuplicates(Match,["Id"]);
    }).then(function() {
        status.success("cleanAllDuplicatedEntries completed successfully.");
    }, function(error) {
        status.error("cleanAllDuplicatedEntries, error " + JSON.stringify(error));
    });
});


Parse.Cloud.job("UpdatePartidasAtuais", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    mcs.updatePartidasAtuais(request).then(function(){
        status.success("UpdatePartidasAtuais completed successfully.");
    }, function(error) {
        status.error("UpdatePartidasAtuais, error " + JSON.stringify(error));
    });
});


Parse.Cloud.job("updateLineUpsPartidasAtuais", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    console.log('******************************************');
    console.log('******* Start updateLineUpsPartidasAtuais');
    
    lns.updateLineUpsPartidasAtuais(request).then(function() {
        console.log('******* End updateLineUpsPartidasAtuais');
        console.log('******************************************');
    }).then(function(){
        status.success("updateLineUpsPartidasAtuais completed successfully.");
    }, function(error) {
        status.error("updateLineUpsPartidasAtuais, error " + JSON.stringify(error));
    });
});

Parse.Cloud.job("CrowlerStatistics", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('******* Start updating Statistics');
    
    console.log('Start updating MatchStatistics');
    mst.updateMatchStatistics(request).then(function(){
        console.log('MatchStatistics Main >>> Start sending push notifications Gols');
        return mst.sendPushNotification(request);
     }).then(function(){
        console.log('Start updating TeamStatistics');
        return tst.updateTeamStatistics(request);
    }).then(function() {
        console.log('******* End updating Statistics');
        console.log('******************************************');
    }).then(function() {
        status.success("CrowlerStatistics completed successfully.");
    }, function(error) {
        status.error("CrowlerStatistics, error " + JSON.stringify(error));
    });
    
});

Parse.Cloud.job("CrowlerLineUpsLibertadores", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('******* Start updating LineUpsLibertadores');
    
    lns.crowlLineUpsLibertadores(request).then(function(){
        console.log('******* Finish LineUpsLibertadores');
        return request;
    }).then(function() {
        status.success("CrowlerLineUpsLibertadores completed successfully.");
    }, function(error) {
        status.error("CrowlerLineUpsLibertadores, error " + JSON.stringify(error));
    });
    
});


Parse.Cloud.job("CrowlerPlayByPlayLibertadoresOldGames", function(request, status) {
    // Set up to modify user data   
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('Narracao >>> Start updating play-by-plays LibertadoresOldGames');
    
    var successMessage = "CrowlerPlayByPlay completed successfully.";
    var query = new Parse.Query(Match);
    query.select('Id', 'Data',
                 'NomeMandante', 'SearchNameMandante',
                 'NomeVisitante', 'SearchNameVisitante');
    query.equalTo("IdCampeonato", 449);
    return query.find(function(results) {
        console.log('Narracao >>> Total de registros: ' + results.length);
        return results;
    }).then(function(results) {
        new_results = []
        _.each(results, function(result) {
            console.log('Narracao >>> Id Partida: ' + result.get("Id"));
            new_results.push(pbp.updatePlayByPlayLibertadores(request, result.get("Id"), result.get("Data"),
                                        result.get("NomeMandante"), result.get("NomeVisitante"),
                                        result.get("SearchNameMandante"), result.get("SearchNameVisitante")));
        })
        console.log('Narracao >>> Total de registros new_results: ' + new_results.length);
        return new_results;
     }).then(function() {
        status.success(successMessage);
     }, function(error) {
        status.error("CrowlerPlayByPlay, error " + JSON.stringify(error));
     });
});


Parse.Cloud.job("PushPlacarTeste", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('******* Start updating PushPlacarTeste');
    
    mst.sendPushNotification(request).then(function(){
        console.log('******* Finish PushPlacarTeste');
        return request;
    }).then(function() {
        status.success("PushPlacarTeste completed successfully.");
    }, function(error) {
        status.error("PushPlacarTeste, error " + JSON.stringify(error));
    });
    
});



Parse.Cloud.job("UpdateMatchStatisticsLibertadores", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('******* Start updating updateMatchStatisticsLibertadores');
    
    mst.updateMatchStatisticsLibertadores(request).then(function(){
        console.log('******* Finish updateMatchStatisticsLibertadores');
        return request;
    }).then(function() {
        status.success("updateMatchStatisticsLibertadores completed successfully.");
    }, function(error) {
        status.error("updateMatchStatisticsLibertadores, error " + JSON.stringify(error));
    });
    
});

Parse.Cloud.job("UpdateEquipePush", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    console.log('******************************************');
    console.log('******* Start UpdateEquipePush');
    
    var query = new Parse.Query(Team);
    query.select('Id', 'IdCampeonato', 'Nome');
    query.containedIn("Nome", ['Corinthians', 'Ponte Preta', 'Palmeiras', 'São Paulo', 'Santos', 'Oeste', 'Bragantino',
        'Grêmio', 'Atlético-MG', 'Vitória-BA', 'Sport', 'Santa Cruz', 'Fluminense', 'Flamengo', 'Figueirense',
        'Coritiba','Cruzeiro','Chapecoense','Botafogo','Atlético-PR','América-MG','Internacional',

        'Goiás','Criciúma','Luverdense','Tupi','Vila Nova-GO','Atlético-GO','Bahia',
        'Avaí','Brasil de Pelotas','Ceará','CRB','Vasco','Paraná','Náutico',
        'Paysandu','Joinville','Londrina','Sampaio Corrêa'
        ]);

    query.find(function(results) {        
        return results;
    }).then(function(results){
        console.log('******* Update size UpdateEquipePush ' + results.length);
        var promises = [];
        var equipeSave = [];
        _.each(results, function(result){
            console.log('******* Update size UpdateEquipePush - Equipe: ' + result.get("Nome"));
            result.set("Push", true);
            equipeSave.push(result);
        });
        promises.push(Parse.Object.saveAll(equipeSave));
        return Parse.Promise.when(promises);
    }).then(function() {
        status.success("updateMatchStatisticsLibertadores completed successfully.");
    }, function(error) {
        status.error("updateMatchStatisticsLibertadores, error " + JSON.stringify(error));
    }); 
});

Parse.Cloud.job("CrowlerScoresSerieD", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    scs.scores(request, 479).then(function(results){
        console.log('******* Finish CrowlerScoresSerieD ' + results);
        return request;
    }).then(function() {
        status.success("CrowlerScoresSerieD completed successfully.");
    }, function(error) {
        status.error("CrowlerScoresSerieD, error " + JSON.stringify(error));
    });

});

Parse.Cloud.job("CrowlerScoresSerieDCount", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Score);
    query.equalTo("IdCampeonato", 479);

    return query.find(function(results) {      
        console.log('*******CrowlerScoresSerieDCount ' + results.length);  
        return results;
    }).then(function() {
        status.success("CrowlerScoresSerieDCount completed successfully.");
    }, function(error) {
        status.error("CrowlerScoresSerieDCount, error " + JSON.stringify(error));
    }); 

});
