var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var GoalScores = Parse.Object.extend('Artilharia');


exports.goalScorers = function(req, championshipId) {
    return ws.updateDataBaseBasedOnAPI(req, "Campeonato/Artilharia",
    {IdCampeonato:championshipId}, GoalScores, updateData, ["IdCampeonato", "IdJogador"], 'IdCampeonato', championshipId,
    function(wsResults){
       if(wsResults != null && wsResults.length > 0){
           for(var i = 0;i < wsResults.length;i++){
               wsResults[i]['IdCampeonato'] = championshipId;
           }
       }
       return wsResults;
    });
};

var updateData = function(bdChamps, wsChamps, isNew){
    var idJogador = parseInt(wsChamps["IdJogador"]),
        jogador = wsChamps["Jogador"],
        idEquipe = parseInt(wsChamps["IdEquipe"]),
        equipe = wsChamps["Equipe"],
        idCampeonato = parseInt(wsChamps["IdCampeonato"]),
        total = parseInt(wsChamps["Total"]);
        
    if(!isNew && idJogador == bdChamps.get('IdJogador') && jogador == bdChamps.get('Jogador') &&
       idEquipe == bdChamps.get('IdEquipe') && equipe == bdChamps.get('Equipe') &&
       idCampeonato == bdChamps.get('IdCampeonato') && total == bdChamps.get('Total')){
           return null;
    }
    bdChamps.set("IdJogador", idJogador);
    bdChamps.set("Jogador", jogador);
    bdChamps.set("IdEquipe", idEquipe);
    bdChamps.set("Equipe", equipe);
    bdChamps.set("IdCampeonato", idCampeonato);
    bdChamps.set("Total", total);
    
    return bdChamps;
};

exports.deleteAll = function(req) {
  return ws.deleteAll(GoalScores);
};
