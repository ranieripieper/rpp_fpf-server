var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var ph = require('cloud/helpers/push_helper.js');
var MatchStatistics = Parse.Object.extend('EstatisticasDaPartida');
var PushPlacar = Parse.Object.extend('PushPlacar');
var Match = Parse.Object.extend('Partida');
var Team = Parse.Object.extend('Equipe');
var Installation = Parse.Object.extend('Installation');

exports.callmeAsAJob = function(isPaidPlan){
    return ws.triggerJobPromise('CrowlerMatchStatistics', isPaidPlan);
};

exports.updateMatchStatistics = function(req) {
    console.log('****************************************** updateMatchStatistics');
    var query = new Parse.Query(Match);
    query.select('Id');
    query.containedIn('IdCampeonato', [444,449,475,476,477,464]);
    query.equalTo('PodePegarEstatisticas', true);
    query.limit(1000);
    
    return query.find(function(results) {        
        return results;
    }).then(function(results){
        var promises = [];
        _.each(results, function(result){
            promises.push(ws.updateDataBaseBasedOnAPI(req, 'Partida/Estatisticas',
                            { IdPartida:result.get('Id') }, MatchStatistics, updateData, ['IdPartida'], null, null,
                            function(wsResults){
                                wsResults["IdPartida"] = result.get('Id');
                                return [wsResults];
                            }));
        });
        promises.push(sendPushNotification(req));
        return Parse.Promise.when(promises);
    });
};

var updateData = function(bdChamps, wsChamps){
    var houseTeam = wsChamps["EquipeMandante"],
        visitorTeam = wsChamps["EquipeVisitante"];
    // console.log('-------------------------------------------estatísticas ------------------------');
    // console.log(houseTeam);
    // console.log(visitorTeam);
    if(houseTeam != null && visitorTeam != null){
        var ballOwnessHouse = houseTeam["PosseBolaEquipe"]["PosseSegundos"],
            ballOwnessVisitor = visitorTeam["PosseBolaEquipe"]["PosseSegundos"],
            percentageBallOwness = ballOwnessHouse + ballOwnessVisitor,
            percentageBallOwnessHouse = (ballOwnessHouse * 100) / percentageBallOwness,
            percentageBallOwnessVisitor = (ballOwnessVisitor * 100) / percentageBallOwness;


        //verifica se envia push do placar
        var query = new Parse.Query(MatchStatistics);
        query.equalTo('IdPartida', wsChamps["IdPartida"]);
        query.limit(1);
        query.first(function(result) {
            //console.log(">>>>>>>>>>>>>>>>>> Old: " + result.get("IdPartida") + " => " + result.get("PlacarMandante") +  " x " + result.get("PlacarVisitante"));
            //console.log(">>>>>>>>>>>>>>>>>> Atu: " + result.get("IdPartida") + " => " + houseTeam["Placar"] +  " x " + visitorTeam["Placar"]);

            if (result != null && result != undefined) {
                var mandanteDif = parseInt(result.get("PlacarMandante")) != parseInt(houseTeam["Placar"]);

                var visitanteDif = parseInt(result.get("PlacarVisitante")) != parseInt(visitorTeam["Placar"]);
                //console.log(">>>>>>>>>>>>>>>>>> visitanteDif " + visitanteDif +  " - mandanteDif " + mandanteDif);

                if (result != null && (mandanteDif || visitanteDif) 
                    && (result.get("PlacarMandante") != undefined && result.get("PlacarVisitante")  != undefined)) {
                    //envia push
                    var pushPlacar = new PushPlacar();
                    pushPlacar.set("IdPartida", wsChamps["IdPartida"]);
                    pushPlacar.set("PlacarMandante", parseInt(houseTeam["Placar"]));
                    pushPlacar.set("PlacarVisitante", parseInt(visitorTeam["Placar"]));
                    pushPlacar.set("Sent", false);

                    if (mandanteDif) {
                        pushPlacar.set("GolMandante", true);
                    } else {
                        pushPlacar.set("GolMandante", false);
                    }
                    pushPlacar.save(null, {
                      success: function(pushPlacar) {
                        // Execute any logic that should take place after the object is saved.
                        console.log('New object created with objectId: ' + pushPlacar.id);
                      },
                      error: function(pushPlacar, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        console.log('Failed to create new object, with error code: ' + error.message);
                      }
                    });

                }
            }
            return result;
        });

        // console.log('ballOwnessHouse: ' + ballOwnessHouse + ' - ballOwnessVisitor: ' + ballOwnessVisitor +
        //             ' - percentageBallOwnessHouse:' + percentageBallOwnessHouse + ' - percentageBallOwnessVisitor:' + percentageBallOwnessVisitor);
        bdChamps.set("IdPartida", wsChamps["IdPartida"]);
        
        bdChamps.set("PasseCertoMandante", parseInt(houseTeam["PasseCerto"]));
        bdChamps.set("PasseErradoMandante", parseInt(houseTeam["PasseErrado"]));
        bdChamps.set("PosseBolaMandante", percentageBallOwnessHouse);
        bdChamps.set("FinalizacaoCertaMandante", parseInt(houseTeam["FinalizacaoCerta"]));
        bdChamps.set("FinalizacaoErradaMandante", parseInt(houseTeam["FinalizacaoErrada"]));
        bdChamps.set("CruzamentoCertoMandante", parseInt(houseTeam["CruzamentoCerto"]));
        bdChamps.set("CruzamentoErradoMandante", parseInt(houseTeam["CruzamentoErrado"]));
        bdChamps.set("DesarmeCertoMandante", parseInt(houseTeam["DesarmeCerto"]));
        bdChamps.set("DesarmeErradoMandante", parseInt(houseTeam["DesarmeErrado"]));
        bdChamps.set("LancamentoCertoMandante", parseInt(houseTeam["LancamentoCerto"]));
        bdChamps.set("LancamentoErradoMandante", parseInt(houseTeam["LancamentoErrado"]));
        bdChamps.set("FaltaCometidaMandante", parseInt(houseTeam["FaltaCometida"]));
        bdChamps.set("FaltaRecebidaMandante", parseInt(houseTeam["FaltaRecebida"]));
        bdChamps.set("EscanteioProMandante", parseInt(houseTeam["EscanteioPro"]));
        bdChamps.set("EscanteioContraMandante", parseInt(houseTeam["EscanteioContra"]));
        bdChamps.set("CartaoAmareloMandante", parseInt(houseTeam["CartaoAmarelo"]));
        bdChamps.set("CartaoVermelhoMandante", parseInt(houseTeam["CartaoVermelho"]));
        bdChamps.set("PenaltiCometidoMandante", parseInt(houseTeam["PenaltiCometido"]));
        bdChamps.set("PenaltiRecebidoMandante", parseInt(houseTeam["PenaltiRecebido"]));
        bdChamps.set("PlacarMandante", parseInt(houseTeam["Placar"]));
        
        bdChamps.set("PasseCertoVisitante", parseInt(visitorTeam["PasseCerto"]));
        bdChamps.set("PasseErradoVisitante", parseInt(visitorTeam["PasseErrado"]));
        bdChamps.set("PosseBolaVisitante", percentageBallOwnessVisitor);
        bdChamps.set("FinalizacaoCertaVisitante", parseInt(visitorTeam["FinalizacaoCerta"]));
        bdChamps.set("FinalizacaoErradaVisitante", parseInt(visitorTeam["FinalizacaoErrada"]));
        bdChamps.set("CruzamentoCertoVisitante", parseInt(visitorTeam["CruzamentoCerto"]));
        bdChamps.set("CruzamentoErradoVisitante", parseInt(visitorTeam["CruzamentoErrado"]));
        bdChamps.set("DesarmeCertoVisitante", parseInt(visitorTeam["DesarmeCerto"]));
        bdChamps.set("DesarmeErradoVisitante", parseInt(visitorTeam["DesarmeErrado"]));
        bdChamps.set("LancamentoCertoVisitante", parseInt(visitorTeam["LancamentoCerto"]));
        bdChamps.set("LancamentoErradoVisitante", parseInt(visitorTeam["LancamentoErrado"]));
        bdChamps.set("FaltaCometidaVisitante", parseInt(visitorTeam["FaltaCometida"]));
        bdChamps.set("FaltaRecebidaVisitante", parseInt(visitorTeam["FaltaRecebida"]));
        bdChamps.set("EscanteioProVisitante", parseInt(visitorTeam["EscanteioPro"]));
        bdChamps.set("EscanteioContraVisitante", parseInt(visitorTeam["EscanteioContra"]));
        bdChamps.set("CartaoAmareloVisitante", parseInt(visitorTeam["CartaoAmarelo"]));
        bdChamps.set("CartaoVermelhoVisitante", parseInt(visitorTeam["CartaoVermelho"]));
        bdChamps.set("PenaltiCometidoVisitante", parseInt(visitorTeam["PenaltiCometido"]));
        bdChamps.set("PenaltiRecebidoVisitante", parseInt(visitorTeam["PenaltiRecebido"]));
        bdChamps.set("PlacarVisitante", parseInt(visitorTeam["Placar"]));

        return bdChamps;
    }
    
    return null;
}


var sendPushNotification = function(request){
    var query = new Parse.Query(PushPlacar);
    query.equalTo('Sent', false);
    query.select('IdPartida', 'PlacarMandante', 'PlacarVisitante', 'IdMandante', 'IdVisitante', 'GolMandante');
    
    console.log('-------- Start sendPushNotification - Gols'); 

    var promises = [];

    return query.find(function(results){
       return results || [];
    }).then(function(results){
        console.log('----- results.length ' + results.length);
        if(results.length > 0) {
            _.each(results, function(result) {
                var queryPartida = new Parse.Query(Match);
                queryPartida.equalTo('Id', result.get('IdPartida'));
                queryPartida.limit(1);
                    promises.push(queryPartida.first(function(partida) {
                            console.log('----- partida return');
                            sendPushNotificationPlacarPartida(result, partida)
                        }));
            });
        }
    }).then(function(results) {
        if(promises.length > 0){
           console.log((promises.length / 2) + '----- PushGol - Push Notification(s) is(are) being sent');
           return Parse.Promise.when(promises);
        } else {
            console.log('----- PushGol - No Push Notifications was sent');
            return results;
        }

    });

    return Parse.Promise.when(promises);
}

var sendPushNotificationPlacarPartida = function(pushPlacar, partida){

    promises = [];
    var placarMandante = pushPlacar.get('PlacarMandante'),
                    placarVisitante = pushPlacar.get('PlacarVisitante'),
                    golMandante = pushPlacar.get('GolMandante')
                    idPartida = pushPlacar.get('IdPartida');
                
    if (partida != null) {
        var timegol = "";
        if (golMandante) {
            timegol = partida.get('NomeMandante');
        } else {
            timegol = partida.get('NomeVisitante');
        }
        message = 'Goool do ' + timegol + 
                    '. Agora, ' + partida.get('NomeMandante') + ' ' + placarMandante + ' x ' + 
                    placarVisitante + ' ' + partida.get('NomeVisitante') + '.';
       
        var channels = [partida.get('SearchNameMandante'), partida.get('SearchNameVisitante')];

        //console.log('-------- message: ' + message + " - channels: " +  channels);

        //salva placar da partida
        partida.set('PlacarMandante', placarMandante);
        partida.set('PlacarVisitante', placarVisitante);                            
        partida.save();

        //envia o push
        promises.push(ph.processPush(channels, message, partida.get("Id"), partida.get('IdCampeonato')));

        pushPlacar.set('Sent', true);
        promises.push(pushPlacar.save());
    }

    return Parse.Promise.when(promises);
};

exports.updateMatchStatisticsLibertadores = function(req) {
    console.log('****************************************** updateMatchStatisticsLibertadores');
    var query = new Parse.Query(Match);
    query.select('Id');
    query.containedIn('IdCampeonato', [449]);
    query.limit(1000);
    
    return query.find(function(results) {
        return results;
    }).then(function(results){
        var promises = [];
        _.each(results, function(result){
            promises.push(ws.updateDataBaseBasedOnAPI(req, 'Partida/Estatisticas',
                            { IdPartida:result.get('Id') }, MatchStatistics, updateDataNoPush, ['IdPartida'], null, null,
                            function(wsResults){
                                wsResults["IdPartida"] = result.get('Id');
                                return [wsResults];
                            }));
        });
        promises.push(sendPushNotification(req));
        return Parse.Promise.when(promises);
    });
};

var updateDataNoPush = function(bdChamps, wsChamps){
    var houseTeam = wsChamps["EquipeMandante"],
        visitorTeam = wsChamps["EquipeVisitante"];
    // console.log('-------------------------------------------estatísticas ------------------------');
    // console.log(houseTeam);
    // console.log(visitorTeam);
    if(houseTeam != null && visitorTeam != null){
        var ballOwnessHouse = houseTeam["PosseBolaEquipe"]["PosseSegundos"],
            ballOwnessVisitor = visitorTeam["PosseBolaEquipe"]["PosseSegundos"],
            percentageBallOwness = ballOwnessHouse + ballOwnessVisitor,
            percentageBallOwnessHouse = (ballOwnessHouse * 100) / percentageBallOwness,
            percentageBallOwnessVisitor = (ballOwnessVisitor * 100) / percentageBallOwness;

        // console.log('ballOwnessHouse: ' + ballOwnessHouse + ' - ballOwnessVisitor: ' + ballOwnessVisitor +
        //             ' - percentageBallOwnessHouse:' + percentageBallOwnessHouse + ' - percentageBallOwnessVisitor:' + percentageBallOwnessVisitor);
        bdChamps.set("IdPartida", wsChamps["IdPartida"]);
        
        bdChamps.set("PasseCertoMandante", parseInt(houseTeam["PasseCerto"]));
        bdChamps.set("PasseErradoMandante", parseInt(houseTeam["PasseErrado"]));
        bdChamps.set("PosseBolaMandante", percentageBallOwnessHouse);
        bdChamps.set("FinalizacaoCertaMandante", parseInt(houseTeam["FinalizacaoCerta"]));
        bdChamps.set("FinalizacaoErradaMandante", parseInt(houseTeam["FinalizacaoErrada"]));
        bdChamps.set("CruzamentoCertoMandante", parseInt(houseTeam["CruzamentoCerto"]));
        bdChamps.set("CruzamentoErradoMandante", parseInt(houseTeam["CruzamentoErrado"]));
        bdChamps.set("DesarmeCertoMandante", parseInt(houseTeam["DesarmeCerto"]));
        bdChamps.set("DesarmeErradoMandante", parseInt(houseTeam["DesarmeErrado"]));
        bdChamps.set("LancamentoCertoMandante", parseInt(houseTeam["LancamentoCerto"]));
        bdChamps.set("LancamentoErradoMandante", parseInt(houseTeam["LancamentoErrado"]));
        bdChamps.set("FaltaCometidaMandante", parseInt(houseTeam["FaltaCometida"]));
        bdChamps.set("FaltaRecebidaMandante", parseInt(houseTeam["FaltaRecebida"]));
        bdChamps.set("EscanteioProMandante", parseInt(houseTeam["EscanteioPro"]));
        bdChamps.set("EscanteioContraMandante", parseInt(houseTeam["EscanteioContra"]));
        bdChamps.set("CartaoAmareloMandante", parseInt(houseTeam["CartaoAmarelo"]));
        bdChamps.set("CartaoVermelhoMandante", parseInt(houseTeam["CartaoVermelho"]));
        bdChamps.set("PenaltiCometidoMandante", parseInt(houseTeam["PenaltiCometido"]));
        bdChamps.set("PenaltiRecebidoMandante", parseInt(houseTeam["PenaltiRecebido"]));
        bdChamps.set("PlacarMandante", parseInt(houseTeam["Placar"]));
        
        bdChamps.set("PasseCertoVisitante", parseInt(visitorTeam["PasseCerto"]));
        bdChamps.set("PasseErradoVisitante", parseInt(visitorTeam["PasseErrado"]));
        bdChamps.set("PosseBolaVisitante", percentageBallOwnessVisitor);
        bdChamps.set("FinalizacaoCertaVisitante", parseInt(visitorTeam["FinalizacaoCerta"]));
        bdChamps.set("FinalizacaoErradaVisitante", parseInt(visitorTeam["FinalizacaoErrada"]));
        bdChamps.set("CruzamentoCertoVisitante", parseInt(visitorTeam["CruzamentoCerto"]));
        bdChamps.set("CruzamentoErradoVisitante", parseInt(visitorTeam["CruzamentoErrado"]));
        bdChamps.set("DesarmeCertoVisitante", parseInt(visitorTeam["DesarmeCerto"]));
        bdChamps.set("DesarmeErradoVisitante", parseInt(visitorTeam["DesarmeErrado"]));
        bdChamps.set("LancamentoCertoVisitante", parseInt(visitorTeam["LancamentoCerto"]));
        bdChamps.set("LancamentoErradoVisitante", parseInt(visitorTeam["LancamentoErrado"]));
        bdChamps.set("FaltaCometidaVisitante", parseInt(visitorTeam["FaltaCometida"]));
        bdChamps.set("FaltaRecebidaVisitante", parseInt(visitorTeam["FaltaRecebida"]));
        bdChamps.set("EscanteioProVisitante", parseInt(visitorTeam["EscanteioPro"]));
        bdChamps.set("EscanteioContraVisitante", parseInt(visitorTeam["EscanteioContra"]));
        bdChamps.set("CartaoAmareloVisitante", parseInt(visitorTeam["CartaoAmarelo"]));
        bdChamps.set("CartaoVermelhoVisitante", parseInt(visitorTeam["CartaoVermelho"]));
        bdChamps.set("PenaltiCometidoVisitante", parseInt(visitorTeam["PenaltiCometido"]));
        bdChamps.set("PenaltiRecebidoVisitante", parseInt(visitorTeam["PenaltiRecebido"]));
        bdChamps.set("PlacarVisitante", parseInt(visitorTeam["Placar"]));

        console.log('****************************************** updateDataNoPush ' + wsChamps["IdPartida"]);

        return bdChamps;
    }
    
    return null;
}

exports.sendPushNotification = sendPushNotification;

