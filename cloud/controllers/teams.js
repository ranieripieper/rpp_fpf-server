var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var Team = Parse.Object.extend('Equipe');

exports.teams = function(req, championshipId) {
    return ws.updateDataBaseBasedOnAPI(req, "Equipe/EquipesCampeonato",
                {IdCampeonato:championshipId}, Team, updateData, ["Id"], 'IdCampeonato', championshipId,
    function(results){
        if(results != null && results.length > 0){
            _.each(results, function(result){
                result['IdCampeonato'] = championshipId;
            });
            return results;
        }
        return results;
    });
};

var updateData = function(bdChamps, wsChamps){
    bdChamps.set("Id", parseInt(wsChamps["Id"]));
    bdChamps.set("IdCampeonato", parseInt(wsChamps["IdCampeonato"]));
    bdChamps.set("Nome", wsChamps["Nome"]);
    bdChamps.set("Cidade", wsChamps["Cidade"]);
    bdChamps.set("Pais", wsChamps["Pais"]);
    bdChamps.set("URLLogo", wsChamps["URLLogo"]);
    bdChamps.set("Sigla", wsChamps["Sigla"]);
    bdChamps.set("EquipeFantasia", ws.isBool(wsChamps["NomeCampeonato"]));
    bdChamps.set("TemEstatisticaCompleta", ws.isBool(wsChamps["TemEstatisticaCompleta"]));
    bdChamps.set("Estado", wsChamps["Estado"]);
    return bdChamps;
};


exports.deleteAll = function(req) {
  return ws.deleteAll(Team);
};
