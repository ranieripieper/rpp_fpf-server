var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var LineUp = Parse.Object.extend('Escalacao');
var Match = Parse.Object.extend('Partida');
var PlayByPlay = Parse.Object.extend('Narracao');

var updateLineUps = function(req, matchId){
    return ws.updateDataBaseBasedOnAPI(req, "Partida/Escalacao",
    { IdPartida:matchId }, LineUp, updateData, ["IdMandante","IdVisitante","IdPartida"], 'IdPartida', matchId, function(wsResults){
        if(wsResults != null){
            wsResults["IdPartida"] = matchId;
            if(wsResults["IdMandante"] != null && wsResults["Mandante"] != null &&
               wsResults["IdVisitante"] != null && wsResults["Visitante"] != null){
                return [wsResults];   
            }
        }
        
        return [];
    });
};

exports.updateLineUpsPartidasAtuais = function(req){
    var todayDateInit = new Date();
    todayDateInit.setHours(0);
    todayDateInit.setMinutes(0);
    todayDateInit.setSeconds(0);
    var todayDateEnd = new Date();
    todayDateEnd.setHours(23);
    todayDateEnd.setMinutes(59);
    todayDateEnd.setSeconds(59);

    var query = new Parse.Query(Match);
    query.select("Id", "Data");
    query.equalTo('Comecou', false);
    query.greaterThan("Data", todayDateInit);
    query.lessThan("Data", todayDateEnd);
    query.limit(1000);
    return query.find().then(function(results){
        if(results != null){
            var promises = [];
            for(var i = 0;i < results.length;i++){
                var result = results[i];
                promises.push(updateLineUps(req, result.get("Id")));
            }
            return Parse.Promise.when(promises);
        } 
        return results;
    });;

};

var updateData = function(bdChamps, wsChamps, isNew){
    bdChamps.set("IdMandante", parseInt(wsChamps["IdMandante"]));
    bdChamps.set("Mandante", wsChamps["Mandante"]);
    bdChamps.set("IdVisitante", parseInt(wsChamps["IdVisitante"]));
    bdChamps.set("Visitante", wsChamps["Visitante"]);
    bdChamps.set("IdPartida", parseInt(wsChamps["IdPartida"]));
    
    if(isNew){
        bdChamps.set("PushEnviado", false);
    }
    return bdChamps;
};

exports.crowlLineUpsThaHasBegan = function(request, status) {
    var query = new Parse.Query(Match);
    query.select("Id", "Data");
    query.ascending('Id');
    query.equalTo('Comecou', true);
    query.limit(1000);
    return query.find().then(function(results){
        return handleLineUps(request, results);
    });;
};

exports.crowlLineUpsUnfinished = function(request, status) {
    var query = new Parse.Query(Match);
    query.select("Id", "Data");
    query.ascending('Id');
    query.equalTo('Rolando', true);
    query.limit(1000);
    return query.find().then(function(results){
        return handleLineUps(request, results);
    });;
};

exports.crowlLineUpsLibertadores = function(request) {
    var query = new Parse.Query(Match);
    query.select("Id", "Data");
    query.ascending('Id');
    query.equalTo('Comecou', true);
    query.equalTo('IdCampeonato', 449);
    query.limit(1000);
    return query.find().then(function(results){
        var promises = [];
        for(var i = 0;i < results.length;i++){
            var result = results[i];
            promises.push(updateLineUps(request, result.get("Id")));
        }
        return Parse.Promise.when(promises);
    });;
};


var handleLineUps = function(request, results) {
    if(results != null){
        var todayDate = new Date();
        var _2DaysAgo = new Date();
        _2DaysAgo.setDate(_2DaysAgo.getDate() - 2);
        todayDate.setHours(23);
        todayDate.setMinutes(59);
        var promises = [];
        var lastId = 0;
        for(var i = 0;i < results.length;i++){
            var result = results[i];
            if(lastId != result.get('Id')){
                lastId = result.get('Id');
                if(result.get("Data") <= todayDate){
                    promises.push(updateLineUps(request, result.get("Id")));
                }
            }
        }
        return Parse.Promise.when(promises);
    }

    return results;
};

exports.updateLineUps = updateLineUps;
exports.updateAllLineUpsBasedOnPlayByPlay = function(request){
    // Update on a replacement
    var query = new Parse.Query(PlayByPlay);
    query.containedIn('AcaoImportante', ['Começo de jogo', 'Substituição', 'Fim de jogo']);
    query.equalTo('PushEnviado', false);
    
    return query.find(function(results){
        return results;
    }).then(function(results){
        if(results != null && results.length > 0){
            var promises = [];
            var matchIds = [], matchId;
            _.each(results, function(result){
                matchId = result.get('IdPartida');
                
                if(result.get('AcaoImportante') == 'Substituição'){
                    result.set("PushEnviado", true);
                    promises.push(result.save());
                }
                
                if(matchIds.indexOf(matchId) == -1){
                    promises.push(updateLineUps(request, matchId));
                    matchIds.push(matchId);
                }
            });
            
            if(promises.length > 0){
                console.log(promises.length + ' Lineup(s) is(are) being updated');
                return Parse.Promise.when(promises);
            }
        }
        
        console.log('No Lineups was updated');
        
        return results;
    });
};
exports.deleteAll = function(req) {
  return ws.deleteAll(LineUp);
};
