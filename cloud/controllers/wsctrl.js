var _ = require('underscore');
var moment = require('moment');

var config = {
  baseApiUrl:"http://apifutebol.footstats.com.br/V2/api/",
  defaultHeaders:{
    'Content-Type': 'application/json;charset=utf-8'
  },
  defaultQueryParams:{
    lang:"pt-br",
    Token:"2edf7cff-3be"
  }

};

var requestFactory = function(url,params,headers){
  //console.log('url > ' + config.baseApiUrl+url + "-" + params);
  return {
          method:"GET",
          url: config.baseApiUrl+url,
          params: _.extend(config.defaultQueryParams,params),
          headers:_.extend(config.defaultHeaders,headers)
  };
};
var requestTriggerJobFactory = function(jobName, isPaidPlan){
    return {
      method: 'POST',
      url: 'https://api.parse.com/1/jobs/' + jobName,
      headers: {
          'X-Parse-Application-Id' : 'uSL7CdHbcahIAmVVtlfFko2eg15FYS0er39tI2zr',
          'X-Parse-Master-Key': 'bvhLCT5gIvet3UHW4uYCiNNhJLLOozXRc4hRcHZu',
          'Content-Type': 'application/json'
      },
      body : {
          'plan' : isPaidPlan ? 'paid' : 'free'
      }
    };
};
var triggerJobPromise = function(jobName, isPaidPlan){
    return triggerCallPromisse(requestTriggerJobFactory(jobName, isPaidPlan));
}
var triggerCallPromisse = function(request,req){
    return Parse.Cloud.httpRequest(request).then(function(httpResponse) {
        return httpResponse.data;
    },function(httpResponse) {
    });
};

var queryTargetClass = function(targetClass, uniqueIdentifiers, filterKey, filterValue){
    var query = new Parse.Query(targetClass);
    query.ascending(uniqueIdentifiers);
    
    if(filterKey != null){
        query.equalTo(filterKey, filterValue);
    }
    
    query.limit(1000);
    return query.find();
};

var getAll = function getAll(list, newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, updateSaveFunction) {
    promises = [];

    var query = new Parse.Query(targetClass);
    query.ascending(uniqueIdentifiers);
    
    if(filterKey != null){
        query.equalTo(filterKey, filterValue);
    }
    query.limit(1000);
    query.skip(list.length);

    promises.push(
    query.find().then(function (result) {
        list = list.concat(result);
        if (result.length != 1000) {
            updateSaveFunction(newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, list);
            return;
        }

        return getAll(list, newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, updateSaveFunction);
    }));

    return Parse.Promise.when(promises); 
}


var saveUpdateData = function(newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, dbResults){
    updateSaveDataBaseBasedOnNewData(newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, dbResults);
    return null;
}

var updateDataBaseBasedOnNewData = function(newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn){
    return getAll([], newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, saveUpdateData)
}


var updateSaveDataBaseBasedOnNewData = function(newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn, dbResults){
        
    var fnEquals = function(dbResult, wsResult){
        uniqueIdentifiers = uniqueIdentifiers || [];
        
        for(var i = 0;i < uniqueIdentifiers.length;i++){
            if(dbResult.get(uniqueIdentifiers[i]) != wsResult[uniqueIdentifiers[i]]){
                return false;
            }
        }
        
        return true; 
    };

    var items = [], updatedResult, countNew = 0, countUpdate = 0;
    dbResults = dbResults || [];
    newDataItems = newDataItems || [];
    
    if(wrapperFn != null && typeof wrapperFn == 'function'){
        newDataItems = wrapperFn(newDataItems);
    }
    
    if(dbResults.length > 0 && newDataItems.length > 0){
        for(var ndi = 0, foundAny = false; ndi < newDataItems.length; ndi++, foundAny = false){
            for(var dbi = 0; dbi < dbResults.length; dbi++){
                if(fnEquals(dbResults[dbi], newDataItems[ndi])) {
                    updatedResult = updateData(dbResults[dbi], newDataItems[ndi]);
                    if(updatedResult != null){
                        items.push(updatedResult);
                        countUpdate++;
                    }
                    dbResults.splice(dbi--, 1);
                    foundAny = true;
                    break; // ensure that this item will update just once
                }
            }
            
            
            if(!foundAny){
                updatedResult = updateData(new targetClass(), newDataItems[ndi], true);
                if(updatedResult != null){
                    items.push(updatedResult);
                    countNew++;
                }
            }
        }
        
    } else if(newDataItems.length > 0){
        // Creating new items
        for(var ndi = 0;ndi < newDataItems.length;ndi++){
            updatedResult = updateData(new targetClass(), newDataItems[ndi], true);
            if(updatedResult != null){
                items.push(updatedResult);
                countNew++;
            }
        }
    }
        
    return saveObjectsPromisse(items, targetClass);

};
/*
var updateDataBaseBasedOnNewData = function(newDataItems, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn){
    var fnEquals = function(dbResult, wsResult){
        uniqueIdentifiers = uniqueIdentifiers || [];
            
        for(var i = 0;i < uniqueIdentifiers.length;i++){
            if(dbResult.get(uniqueIdentifiers[i]) != wsResult[uniqueIdentifiers[i]]){
                return false;
            }
        }
        
        return true;
    };
    return queryTargetClass(targetClass, uniqueIdentifiers, filterKey, filterValue).then(function(dbResults){
        
        var items = [], updatedResult, countNew = 0, countUpdate = 0;
        dbResults = dbResults || [];
        newDataItems = newDataItems || [];
        
        if(wrapperFn != null && typeof wrapperFn == 'function'){
            newDataItems = wrapperFn(newDataItems);
        }
        
        if(dbResults.length > 0 && newDataItems.length > 0){
            
            for(var ndi = 0, foundAny = false; ndi < newDataItems.length; ndi++, foundAny = false){
                for(var dbi = 0; dbi < dbResults.length; dbi++){
                    if(fnEquals(dbResults[dbi], newDataItems[ndi])) {
                        updatedResult = updateData(dbResults[dbi], newDataItems[ndi]);
                        if(updatedResult != null){
                            items.push(updatedResult);
                            countUpdate++;
                        }
                        dbResults.splice(dbi--, 1);
                        foundAny = true;
                        break; // ensure that this item will update just once
                    }
                }
                
                
                if(!foundAny){
                    updatedResult = updateData(new targetClass(), newDataItems[ndi], true);
                    if(updatedResult != null){
                        items.push(updatedResult);
                        countNew++;
                    }
                }
            }
            
        } else if(newDataItems.length > 0){
            // Creating new items
            for(var ndi = 0;ndi < newDataItems.length;ndi++){
                updatedResult = updateData(new targetClass(), newDataItems[ndi], true);
                if(updatedResult != null){
                    items.push(updatedResult);
                    countNew++;
                }
            }
        }
            
            
        return saveObjectsPromisse(items, targetClass);
    });
};


var saveObjectsPromisse = function(items, targetClass){
    if(items.length > 0){
        var promises = [];
        _.each(items, function(result) {          
            promises.push(result.save());
        });
        
        return Parse.Promise.when(promises).then(function(results){
            console.log(promises.length + ' ' + targetClass.className + ' object(s) was(were) saved');
            return results;
        });
    }
    
    // console.log('No ' + targetClass.className + ' object(s) was (were) saved');
    return Parse.Promise.as();
};
*/
var saveObjectsPromisse = function(items, targetClass){
    if(items.length > 0){
        var promises = [];

        console.log('Saving...... ' + targetClass.className + ' ' + items.length + ' object(s)');

        if (items.length > 60) {
            itemsSave = [];
            _.each(items, function(result) {   
                if (itemsSave.length >= 60) {
                    promises.push(Parse.Object.saveAll(itemsSave));
                    console.log('Saving (Split)...... ' + targetClass.className + ' ' + itemsSave.length + ' object(s)');
                    itemsSave = [];
                }      
                itemsSave.push(result);
                
            });
            console.log('Saving (Split)...... ' + targetClass.className + ' ' + itemsSave.length + ' object(s)');
            promises.push(Parse.Object.saveAll(itemsSave));
        } else {
            promises.push(Parse.Object.saveAll(items));
        }

        return Parse.Promise.when(promises); 

    } else {
        console.log('No ' + targetClass.className + ' object(s) was (were) saved');
        return Parse.Promise.as();
    }
};

var deleteAll = function(targetClass) {
    var query = new Parse.Query(targetClass);
    var limit = 1000;
    query.limit(limit);
    return query.find().then(function(objs) {
        return Parse.Object.destroyAll(objs).then(function(){
            if(objs != null && objs.length >= limit)
                return deleteAll(targetClass);
        });
    });
};

exports.isValidChamp = function(champId){
    var validsChamps = [479, 478, 475, 476, 477, 464, 444, 486, 456, 461, 449, 495];
    
    return validsChamps.indexOf(champId) != -1;
}

exports.requestFactory = requestFactory;
exports.triggerCallPromisse = triggerCallPromisse;
exports.triggerJobPromise = triggerJobPromise;
exports.updateDataBaseBasedOnNewData = updateDataBaseBasedOnNewData;

exports.updateDataBaseBasedOnAPI = function(req, url, apiParams, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn){
    var request = requestFactory(url, apiParams); 
    
    return triggerCallPromisse(request,req).then(function(wsResults){
        return updateDataBaseBasedOnNewData(wsResults, targetClass, updateData, uniqueIdentifiers, filterKey, filterValue, wrapperFn);
    });
};


exports.saveObjectsPromisse = saveObjectsPromisse;

exports.isBool = function(value){
  return parseInt(value) != 0;
};

exports.deleteAll = deleteAll;

exports.formatDate = function(value){
  
    if(value == null && value.length != 19)
        return null;
    
    //parseInt(x, 10) - http://stackoverflow.com/questions/7318385/problems-with-javascript-parseint
    var day = parseInt(value.substr(0, 2), 10),
        month = parseInt(value.substr(3, 2), 10) - 1,
        year = parseInt(value.substr(6, 4), 10),
        hour = parseInt(value.substr(11, 2), 10),
        minute = parseInt(value.substr(14, 2), 10),
        second = parseInt(value.substr(17, 2), 10);
    
    if(isNaN(day) || isNaN(month) || isNaN(year)
       || isNaN(hour) || isNaN(minute) || isNaN(second))
       return null;


    return new Date(year, month, day, hour, minute, second);
}