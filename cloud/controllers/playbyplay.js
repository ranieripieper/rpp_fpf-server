var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var ph = require('cloud/helpers/push_helper.js');
var PlayByPlay = Parse.Object.extend('Narracao');
var Match = Parse.Object.extend('Partida');

exports.updatePlayByPlayLibertadores = function(req, matchId, date, hostTeam, visitantTeam, hostTeamChannel, visitantTeamChannel){
    return ws.updateDataBaseBasedOnAPI(req, "Partida/NarracaoMinMin",
        { IdPartida: matchId }, PlayByPlay, updateDataPushSent, ["Id"], 'IdPartida', matchId, function (wsResults){
            if(wsResults != null && wsResults["Narracoes"] != null && wsResults["Narracoes"].length > 0){
                var pbps = wsResults["Narracoes"];
                _.each(pbps, function(pbp){
                    pbp['PlacarMandante'] = parseInt(wsResults['PlacarMandante']);
                    pbp['PlacarVisitante'] = parseInt(wsResults['PlacarVisitante']);
                    pbp['PlacarPenaltisMandante'] = wsResults['PlacarPenaltisMandante'];
                    pbp['PlacarPenaltisVisitante'] = wsResults['PlacarPenaltisVisitante'];
                    pbp['NomeMandante'] = hostTeam;
                    pbp['NomeVisitante'] = visitantTeam;
                    pbp['PeriodoAtual'] = wsResults['PeriodoAtual'];
                    pbp['SearchNameMandante'] = hostTeamChannel;
                    pbp['SearchNameVisitante'] = visitantTeamChannel;
                });
                return pbps;
            }
            return [];
        });
};


exports.updatePlayByPlay = function(req, matchId, date, hostTeam, visitantTeam, hostTeamChannel, visitantTeamChannel){
  
    return ws.updateDataBaseBasedOnAPI(req, "Partida/NarracaoMinMin",
        { IdPartida: matchId }, PlayByPlay, updateData, ["Id"], 'IdPartida', matchId, function (wsResults){
            if(wsResults != null && wsResults["Narracoes"] != null && wsResults["Narracoes"].length > 0){
                var pbps = wsResults["Narracoes"];
                _.each(pbps, function(pbp){
                    pbp['PlacarMandante'] = parseInt(wsResults['PlacarMandante']);
                    pbp['PlacarVisitante'] = parseInt(wsResults['PlacarVisitante']);
                    pbp['PlacarPenaltisMandante'] = wsResults['PlacarPenaltisMandante'];
                    pbp['PlacarPenaltisVisitante'] = wsResults['PlacarPenaltisVisitante'];
                    pbp['NomeMandante'] = hostTeam;
                    pbp['NomeVisitante'] = visitantTeam;
                    pbp['PeriodoAtual'] = wsResults['PeriodoAtual'];
                    pbp['SearchNameMandante'] = hostTeamChannel;
                    pbp['SearchNameVisitante'] = visitantTeamChannel;
                });
                return pbps;
            }
            return [];
        });
};

var updateDataPushSent = function(bdChamps, wsChamps, isNew) {
    bdChamps.set("Id", parseInt(wsChamps["Id"]));
    bdChamps.set("IdPartida", parseInt(wsChamps["Idpartida"]));
    bdChamps.set("IdEquipe", parseInt(wsChamps["IdEquipe"]));
    bdChamps.set("NomeEquipe", wsChamps["NomeEquipe"]);
    bdChamps.set("IdJogador", parseInt(wsChamps["IdJogador"]));
    bdChamps.set("NomeJogador", wsChamps["NomeJogador"]);
    bdChamps.set("Periodo", wsChamps["Periodo"]);
    bdChamps.set("Momento", wsChamps["Momento"]);
    bdChamps.set("Descricao", wsChamps["Descricao"]);
    bdChamps.set("AcaoImportante", wsChamps["AcaoImportante"]);
    bdChamps.set("IdSubstituto", parseInt(wsChamps["IdSubstituto"]));
    bdChamps.set("NomeSubstituto", wsChamps["NomeSubstituto"]);
    bdChamps.set('PlacarMandante', parseInt(wsChamps['PlacarMandante']));
    bdChamps.set('PlacarVisitante', parseInt(wsChamps['PlacarVisitante']));
    bdChamps.set('PlacarPenaltisMandante', wsChamps['PlacarPenaltisMandante']);
    bdChamps.set('PlacarPenaltisVisitante', wsChamps['PlacarPenaltisVisitante']);
    bdChamps.set('NomeMandante', wsChamps['NomeMandante']);
    bdChamps.set('NomeVisitante', wsChamps['NomeVisitante']);
    bdChamps.set('PeriodoAtual', wsChamps['PeriodoAtual']);
    bdChamps.set('SearchNameMandante', wsChamps['SearchNameMandante']);
    bdChamps.set('SearchNameVisitante', wsChamps['SearchNameVisitante']);
    
    bdChamps.set("PushEnviado", true);
     
    return bdChamps;
};

var updateData = function(bdChamps, wsChamps, isNew){
    bdChamps.set("Id", parseInt(wsChamps["Id"]));
    bdChamps.set("IdPartida", parseInt(wsChamps["Idpartida"]));
    bdChamps.set("IdEquipe", parseInt(wsChamps["IdEquipe"]));
    bdChamps.set("NomeEquipe", wsChamps["NomeEquipe"]);
    bdChamps.set("IdJogador", parseInt(wsChamps["IdJogador"]));
    bdChamps.set("NomeJogador", wsChamps["NomeJogador"]);
    bdChamps.set("Periodo", wsChamps["Periodo"]);
    bdChamps.set("Momento", wsChamps["Momento"]);
    bdChamps.set("Descricao", wsChamps["Descricao"]);
    bdChamps.set("AcaoImportante", wsChamps["AcaoImportante"]);
    bdChamps.set("IdSubstituto", parseInt(wsChamps["IdSubstituto"]));
    bdChamps.set("NomeSubstituto", wsChamps["NomeSubstituto"]);
    bdChamps.set('PlacarMandante', parseInt(wsChamps['PlacarMandante']));
    bdChamps.set('PlacarVisitante', parseInt(wsChamps['PlacarVisitante']));
    bdChamps.set('PlacarPenaltisMandante', wsChamps['PlacarPenaltisMandante']);
    bdChamps.set('PlacarPenaltisVisitante', wsChamps['PlacarPenaltisVisitante']);
    bdChamps.set('NomeMandante', wsChamps['NomeMandante']);
    bdChamps.set('NomeVisitante', wsChamps['NomeVisitante']);
    bdChamps.set('PeriodoAtual', wsChamps['PeriodoAtual']);
    bdChamps.set('SearchNameMandante', wsChamps['SearchNameMandante']);
    bdChamps.set('SearchNameVisitante', wsChamps['SearchNameVisitante']);
    
    if(isNew){
        bdChamps.set("PushEnviado", false);
    } else {
        return null;
    }
    
    return bdChamps;
};
exports.sendPushNotification = function(request){
    var query = new Parse.Query(PlayByPlay);
    query.containedIn('AcaoImportante', ['Começo de jogo', /*'Gol',*/ 'Fim de jogo']);
    query.equalTo('PushEnviado', false);
    query.select('IdPartida', 'AcaoImportante',
                 'NomeMandante','NomeVisitante',
                 'SearchNameVisitante', 'SearchNameMandante',
                 'PushEnviado', 'NomeEquipe',
                 'PlacarMandante', 'PlacarVisitante');

    return query.find(function(results){
       return results || [];
    }).then(function(results){
       if(results.length > 0){
           var message, promises = [];
           
           _.each(results, function(result) {
                var placar1 = result.get('PlacarMandante'),
                    placar2 = result.get('PlacarVisitante'),
                    time1 = result.get('NomeMandante'),
                    time2 = result.get('NomeVisitante'),
                    timegol = result.get('NomeEquipe');
                    
                //busca partida
                var query = new Parse.Query(Match);
                query.equalTo('IdPartida', result["IdPartida"]);
                query.limit(1);
                promises.push(
                    query.first(function(partida) {
                        var mustSend = false;
                        switch(result.get('AcaoImportante')){
                            case 'Começo de jogo':
                                message = 'Começou! Rolou a bola para ' + time1 + ' x ' + time2 + '.';
                                mustSend = true;
                                break;
                            case 'Gol':
                                message = 'Goool do ' + timegol + '.'; //+ '. Agora, ' + time1 + ' ' + placar1 + ' x ' + placar2 + ' ' + time2 + '.';
                                mustSend = true;
                                break;
                            case 'Fim de jogo':
                                message = 'Fim de jogo! ' + time1 + ' ' + placar1 + ' x ' + placar2 + ' ' + time2 + '.';
                                mustSend = true;
                                break;
                        }
                       
                       if(mustSend) {
                            console.log("message>>> " + ' message');
                            sendPushNotificationNarracaoPartida(result, message, partida);
                            mustSend = false;
                       }
                   }));
            });

           if(promises.length > 0){
               console.log((promises.length / 2) + ' Push Notification(s) is(are) being sent');
               return Parse.Promise.when(promises);
           }
       }
       
       console.log('No Push Notifications was sent');
       return results;
    });
};

var sendPushNotificationNarracaoPartida = function(narracao, message, partida){
    promises = []
    var channels = [narracao.get('SearchNameMandante'), narracao.get('SearchNameVisitante')];
    promises.push(ph.processPush(channels, message, narracao.get('IdPartida'), partida.get("IdCampeonato")));
    narracao.set('PushEnviado', true);
    promises.push(narracao.save());

    return Parse.Promise.when(promises);
};

exports.deleteAll = function(req, res) {
  return ws.deleteAll(PlayByPlay);
};
