var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var Championship = Parse.Object.extend('Campeonato');

exports.championships = function(req) {
    return ws.updateDataBaseBasedOnAPI(req, "Campeonato/ListarCampeonatos", null, Championship, updateData, ["Id"]);
};

var updateData = function(bdChamps, wsChamps){
    var qtdRodadas = parseInt(wsChamps["QtdRodadas"]),
        rodadaAtual = parseInt(wsChamps["RodadaAtual"]),
        champId = parseInt(wsChamps["Id"]);
        
    bdChamps.set("Id", champId);
    bdChamps.set("Nome", wsChamps["Nome"]);
    bdChamps.set("Temporada", wsChamps["Temporada"]);
    bdChamps.set("Categoria", wsChamps["Categoria"]);
    bdChamps.set("TipoColeta", wsChamps["TipoColeta"]);
    bdChamps.set("FaseAtual", wsChamps["FaseAtual"]);
    bdChamps.set("Local", wsChamps["Local"]);
    bdChamps.set("TemClassificacao", ws.isBool(wsChamps["TemClassificacao"]));
    bdChamps.set("RodadaAtual", rodadaAtual);
    bdChamps.set("QtdRodadas", qtdRodadas);
    bdChamps.set("ClassificacaoGrupo", ws.isBool(wsChamps["ClassificacaoGrupo"]));
    bdChamps.set("Finalizado", rodadaAtual >= qtdRodadas);
    
    if(!ws.isValidChamp(champId)){
        return null;
    }
    
    return bdChamps;
};



exports.deleteAll = function(req) {
  return ws.deleteAll(Championship);
};
