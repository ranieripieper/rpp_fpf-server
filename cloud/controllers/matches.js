var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var Match = Parse.Object.extend('Partida');
var PlayByPlay = Parse.Object.extend('Narracao');
var dcs = require('cloud/helpers/diacritics.js');
var ph = require('cloud/helpers/push_helper.js');

exports.matches = function(req, championshipId) {
    console.log('>>> Match Update: =>' + championshipId);
    return ws.updateDataBaseBasedOnAPI(req, "Partida/PartidasCampeonato",
        { IdCampeonato:championshipId }, Match, updateData, ["Id"], 'IdCampeonato', championshipId);
};
var convertToBrazilDate = function(brazilDate, minutesToAdd){
    var result = new Date(brazilDate.getUTCFullYear(), brazilDate.getUTCMonth(), brazilDate.getUTCDate(), brazilDate.getUTCHours(), brazilDate.getUTCMinutes(), brazilDate.getUTCSeconds());
    result.setHours(result.getHours() - 3);
    
    if(minutesToAdd != null && minutesToAdd != 0){
        result.setMinutes(result.getMinutes() + minutesToAdd);
    }
    return result;
};
var updateData = function(bdChamps, wsChamps){
    var someMinutesAgoBrazil = convertToBrazilDate(new Date());
    someMinutesAgoBrazil.setMinutes(someMinutesAgoBrazil.getMinutes() - 180);
    
    var matchDate = ws.formatDate(wsChamps["Data"]),
        champId = parseInt(wsChamps["IdCampeonato"]),
        hasPlayByPlay = ws.isBool(wsChamps["TemNarracao"]),
        hasStatistics = ws.isBool(wsChamps["TemEstatistica"]),
        justNowBrazil = convertToBrazilDate(new Date()),
        currentPeriod = wsChamps["PeriodoAtual"],
        isFinished = currentPeriod == 'Partida encerrada',
        isCanceled = currentPeriod == 'Partida cancelada',
        isInterrupted = currentPeriod == 'Partida interrompida',
        hasBegan = currentPeriod != 'Partida não iniciada' && !isCanceled && matchDate <= justNowBrazil,
        isRunning = hasBegan && !isInterrupted && !isFinished && matchDate >= someMinutesAgoBrazil;    
    
    //if (parseInt(wsChamps["Id"]) == 108233 || parseInt(wsChamps["Id"]) == 108259) {
    //  console.log('PArtida : ' + wsChamps["NomeMandante"] + " > " + matchDate + " ===== " + wsChamps["Data"]);
    //}

  //gambiarra - continuar pegando narração após o jogo terminar - Não está atualizando o final de jogo
    var dateNow = convertToBrazilDate(new Date());
    var matchDateEnd = ws.formatDate(wsChamps["Data"]);
    matchDateEnd.setMinutes(matchDateEnd.getMinutes() + 90 + 300);
    var getNarracaoEnd = false;
    if (!isRunning && matchDate >= someMinutesAgoBrazil && dateNow <= matchDateEnd && currentPeriod != 'Partida não iniciada') {
        console.log('>>> Jogo terminou, mas continua pegando narração por alguns minutos. Partida: ' + parseInt(wsChamps["Id"]));
        getNarracaoEnd = true;
    }

    var canGetPlayByPlay = hasPlayByPlay && (isRunning || getNarracaoEnd);
    var canGetStatistics = hasStatistics && (isRunning || getNarracaoEnd);

    //if (parseInt(wsChamps["Id"]) == 108233 || parseInt(wsChamps["Id"]) == 108259) {
    //        console.log('PArtida : ' + wsChamps["NomeMandante"] + " > " + matchDate + " ===== " + wsChamps["Data"]);
    //}
    
    bdChamps.set("Gols", wsChamps["Gols"]);
    bdChamps.set("Id", parseInt(wsChamps["Id"]));
    bdChamps.set("Rodada", parseInt(wsChamps["Rodada"]));
    bdChamps.set("IdEquipeMandante", parseInt(wsChamps["IdEquipeMandante"]));
    bdChamps.set("NomeMandante", wsChamps["NomeMandante"]);
    bdChamps.set("PlacarMandante", parseInt(wsChamps["PlacarMandante"]));
    bdChamps.set("IdEquipeVisitante", parseInt(wsChamps["IdEquipeVisitante"]));
    bdChamps.set("NomeVisitante", wsChamps["NomeVisitante"]);
    bdChamps.set("PlacarVisitante", parseInt(wsChamps["PlacarVisitante"]));
    bdChamps.set("NomeEstadio", wsChamps["NomeEstadio"]);
    bdChamps.set("Cidade", wsChamps["Cidade"]);
    bdChamps.set("PeriodoAtual", currentPeriod);
    bdChamps.set("Data", matchDate);
    bdChamps.set("IdCampeonato", champId);
    bdChamps.set("Fase", wsChamps["Fase"]);
    bdChamps.set("Grupo", wsChamps["Grupo"]);
    bdChamps.set("DataDefinir", ws.isBool(wsChamps["DataDefinir"]));
    bdChamps.set("HoraDefinir", ws.isBool(wsChamps["HoraDefinir"]));
    bdChamps.set("NumeroJogoChave", parseInt(wsChamps["NumeroJogoChave"]));
    bdChamps.set("TemEstatistica", hasStatistics);
    bdChamps.set("TemNarracao", hasPlayByPlay);
    bdChamps.set("Arbitro", wsChamps["Arbitro"]);
    bdChamps.set("TecnicoMandante", wsChamps["TecnicoMandante"]);
    bdChamps.set("TecnicoVisitante", wsChamps["TecnicoVisitante"]);
    bdChamps.set("Publico", parseInt(wsChamps["Publico"]));
    bdChamps.set("Renda", parseFloat(wsChamps["Renda"]));
    bdChamps.set("PlacarPenaltisMandante", parseInt(wsChamps["PlacarPenaltisMandante"]));
    bdChamps.set("PlacarPenaltisVisitante", parseInt(wsChamps["PlacarPenaltisVisitante"]));
    bdChamps.set("JogoNaoNecessario", ws.isBool(wsChamps["Publico"]));

    bdChamps.set("SearchNameVisitante", dcs.removeDiacritics(wsChamps["NomeVisitante"]));
    bdChamps.set("SearchNameMandante", dcs.removeDiacritics(wsChamps["NomeMandante"]));
    bdChamps.set("PodePegarNarracoes", canGetPlayByPlay);
    bdChamps.set("PodePegarEstatisticas", canGetStatistics);
    bdChamps.set("Finalizado", isFinished);
    bdChamps.set("Cancelada", isCanceled);
    bdChamps.set("Interrompida", isInterrupted);
    bdChamps.set("Rolando", isRunning);
    bdChamps.set("Comecou", hasBegan);
    
    
    if(!ws.isValidChamp(champId)){
        return null;
    }
    
    return bdChamps;
};
exports.updatePartidasAtuais = function(request){
    console.log('******* All Current Match(es) is(are) being updated');
    return ws.updateDataBaseBasedOnAPI(request, "Partida/PartidasAtuais", null, Match, updateData, ["Id"]);
}
exports.updateAllMatchesBasedOnPlayByPlay = function(request){
    // Update on end of game
    var query = new Parse.Query(PlayByPlay);
    query.containedIn('AcaoImportante', ['Começo de jogo', 'Gol', 'Fim de jogo']);
    query.equalTo('PushEnviado', false);
    return query.count(function(count){
        return count;
    }).then(function(count){
        if(count > 0){
            console.log(count + ' Match(es) is(are) being updated');
            return ws.updateDataBaseBasedOnAPI(request, "Partida/PartidasAtuais", null, Match, updateData, ["Id"]);
        } else {
            console.log('No Matches was updated');
            return [];
        }
    });
};

exports.deleteAll = function(req) {
  return ws.deleteAll(Match);
};