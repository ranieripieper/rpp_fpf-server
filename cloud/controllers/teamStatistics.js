var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var TeamStatistics = Parse.Object.extend('EstatisticasDoTime');
var Team = Parse.Object.extend('Equipe');
var Match = Parse.Object.extend('Partida');


exports.callmeAsAJob = function(isPaidPlan){
    return ws.triggerJobPromise('CrowlerTeamStatistics', isPaidPlan);
};

exports.updateTeamStatistics = function(req) {
    var query = new Parse.Query(Team);
    query.select('Id', 'IdCampeonato', 'Nome');
    query.equalTo('IdCampeonato', 444);
    query.limit(1000);
    
    return query.find(function(teams) {
        return teams;
    }).then(function(teams){
        var promises = [], teamsStatistics = [];
        console.log(teams.length + ' teams found');
        // Starting iterate for Goals Pros and Against
        _.each(teams, function(team){
            var queryMandante = new Parse.Query(Match),
                queryVisitante = new Parse.Query(Match)
            queryMandante.equalTo('IdEquipeMandante', team.get('Id'));
            queryVisitante.equalTo('IdEquipeVisitante', team.get('Id'));
            
            var queryForTeam = Parse.Query.or(queryMandante, queryVisitante);
            queryForTeam.equalTo('IdCampeonato', 444);
            queryForTeam.equalTo('Finalizado', true);
            queryForTeam.select('IdCampeonato', 'Id', 'Gols',
                                'IdEquipeMandante', 'NomeMandante', 'PlacarMandante',
                                'IdEquipeVisitante', 'NomeVisitante', 'PlacarVisitante');
            var teamId = team.get('Id'),
                teamName = team.get('Nome');
            
            promises.push(
                queryForTeam.find(function(matches){
                    return matches;
                }).then(function(matches){
                    var totalJogosComoMandante = 0, totalJogosComoVisitante = 0,
                        pontosComoMandante = 0, pontosComoVisitante = 0,
                        jogosComoMandante = [], jogosComoVisitante = [],
                        points, goalsPro = [], goalsAgainst = [], goals
                        
                       
                    // Starting iterate for Matches and Points as host or visitor
                    _.each(matches, function(match){
                        var isHostTeam = match.get('IdEquipeMandante') == teamId,
                            isVisitorTeam = match.get('IdEquipeVisitante') == teamId,
                            teamAgainstId = isHostTeam ? match.get('IdEquipeVisitante') : match.get('IdEquipeMandante');
                            
                            
                        goals = match.get('Gols');

                        var goalsProCount = 0,
                            goalsAgainstCount = 0;
                        // Starting iterate for Goals Pros and Against
                        _.each(goals, function(goal){
                            if((teamName == goal['Equipe'] && goal['Tipo'] == 'Favor') ||
                               (teamName != goal['Equipe'] && goal['Tipo'] == 'Contra')) {
                                var playerGoal = getGoalsFromPlayer(goal['Jogador'], goalsPro);
                                
                                goalsProCount++;

                                if(playerGoal != null){
                                    playerGoal['Gols'] = playerGoal['Gols'] + 1;
                                } else {
                                    goalsPro.push({'Jogador': goal['Jogador'], 'Gols': 1});
                                }
                            } else {
                                var teamGoal = getGoalsFromTeam(goal['Equipe'], goalsAgainst);
                                
                                goalsAgainstCount++;
                                
                                if(teamGoal != null){
                                    teamGoal['Gols'] = teamGoal['Gols'] + 1;
                                } else {
                                    goalsAgainst.push({'EquipeId': teamName == goal['Equipe'] ? teamId : teamAgainstId,'Equipe': goal['Equipe'], 'Gols': 1});
                                }
                            }
                        });
                        // Ending iterate for goals

                        if(isHostTeam){
                            points = matchPoints(match.get('PlacarMandante'), match.get('PlacarVisitante'));
                            pontosComoMandante += points;
                            totalJogosComoMandante++;
                            jogosComoMandante.push({
                                'IdEquipe': match.get('IdEquipeVisitante'),
                                'Equipe' : match.get('NomeVisitante'),
                                'GolsPro': goalsProCount,
                                'GolsContra' : goalsAgainstCount
                            });
                        } else if(isVisitorTeam){

                            points = matchPoints(match.get('PlacarVisitante'), match.get('PlacarMandante'));
                            pontosComoVisitante += points;
                            totalJogosComoVisitante++;
                            jogosComoVisitante.push({
                                'IdEquipe' : match.get('IdEquipeMandante'),
                                'Equipe' : match.get('NomeMandante'),
                                'GolsPro' : goalsProCount,
                                'GolsContra' : goalsAgainstCount
                            });
                        }
                        // Ending iterate in Goals Pros and Against
                    });
                    
                    var teamStatistics = {
                        'IdEquipe': teamId,
                        'Equipe': teamName,
                        'TotalJogosComoMandante': totalJogosComoMandante,
                        'TotalJogosComoVisitante': totalJogosComoVisitante,
                        'JogosComoMandante' : jogosComoMandante,
                        'JogosComoVisitante' : jogosComoVisitante,
                        'GolsPro': goalsPro,
                        'GolsContra': goalsAgainst,
                        'PontosComoMandante': pontosComoMandante,
                        'PontosComoVisitante': pontosComoVisitante
                    };
                    
                    teamsStatistics.push(teamStatistics);
                    return teamsStatistics;
                })
            );
        });
        // Ending iterate for teams
        
        return Parse.Promise.when(promises).then(function(statistics){
            console.log('Start saving team statistics');
            return ws.updateDataBaseBasedOnNewData(statistics, TeamStatistics, updateData, ['IdEquipe']);
        });
    });
};

var updateData = function(bdChamps, wsChamps){
    bdChamps.set('IdEquipe', wsChamps['IdEquipe']);
    bdChamps.set('Equipe', wsChamps['Equipe']);
    bdChamps.set('TotalJogosComoMandante', wsChamps['TotalJogosComoMandante']);
    bdChamps.set('TotalJogosComoVisitante', wsChamps['TotalJogosComoVisitante']);
    bdChamps.set('JogosComoMandante', wsChamps['JogosComoMandante']);
    bdChamps.set('JogosComoVisitante', wsChamps['JogosComoVisitante']);
    bdChamps.set('GolsPro', wsChamps['GolsPro']);
    bdChamps.set('GolsContra', wsChamps['GolsContra']);
    bdChamps.set('PontosComoMandante', wsChamps['PontosComoMandante']);
    bdChamps.set('PontosComoVisitante', wsChamps['PontosComoVisitante']);
    
    return bdChamps;
};
var getGoalsFromPlayer = function(playerName, goals){
    var playerGoal = null;
    _.each(goals, function(goal){
       if(goal['Jogador'] == playerName){
           playerGoal = goal;
           return goal;
       } 
    });
    
    return playerGoal;
};

var getGoalsFromTeam = function(teamName, goals){
    var teamGoal = null;
    _.each(goals, function(goal){
       if(goal['Equipe'] == teamName){
           teamGoal = goal;
           return goal;
       } 
    });
    
    return teamGoal;
}
var matchPoints = function(goals1, goals2){
    if(goals1 > goals2){
        return 3;
    } else if(goals1 == goals2){
        return 1;
    }
    
    return 0;
};