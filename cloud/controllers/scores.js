var _ = require('underscore');
var ws = require('cloud/controllers/wsctrl.js');
var Score = Parse.Object.extend('Classificacao');

exports.scores = function(req, championshipId) {
    return ws.updateDataBaseBasedOnAPI(req, "Campeonato/Classificacao",
                {IdCampeonato:championshipId}, Score, updateData, ["IdEquipe","IdCampeonato"], 'IdCampeonato', championshipId);

};

var updateData = function(bdChamps, wsChamps){
    bdChamps.set("IdCampeonato", parseInt(wsChamps["IdCampeonato"]));
    bdChamps.set("NomeCampeonato", wsChamps["NomeCampeonato"]);
    bdChamps.set("IdEquipe", parseInt(wsChamps["IdEquipe"]));
    bdChamps.set("NomeEquipe", wsChamps["NomeEquipe"]);
    bdChamps.set("Taca", wsChamps["Taca"]);
    bdChamps.set("Fase", wsChamps["Fase"]);
    bdChamps.set("Grupo", wsChamps["Grupo"]);
    bdChamps.set("Rodada", parseInt(wsChamps["Rodada"]));
    bdChamps.set("Posicao", parseInt(wsChamps["Posicao"]));
    bdChamps.set("Pontos", parseInt(wsChamps["Pontos"]));
    bdChamps.set("Jogos", parseInt(wsChamps["Jogos"]));
    bdChamps.set("Vitorias", parseInt(wsChamps["Vitorias"]));
    bdChamps.set("Empates", parseInt(wsChamps["Empates"]));
    bdChamps.set("Derrotas", parseInt(wsChamps["Derrotas"]));
    bdChamps.set("GolsPro", parseInt(wsChamps["GolsPro"]));
    bdChamps.set("GolsSofridos", parseInt(wsChamps["GolsSofridos"]));
    bdChamps.set("SaldoGols", parseInt(wsChamps["SaldoGols"]));
    bdChamps.set("VitoriasCasa", parseInt(wsChamps["VitoriasCasa"]));
    bdChamps.set("EmpatesCasa", parseInt(wsChamps["EmpatesCasa"]));
    bdChamps.set("DerrotasCasa", parseInt(wsChamps["DerrotasCasa"]));
    bdChamps.set("VitoriasFora", parseInt(wsChamps["VitoriasFora"]));
    bdChamps.set("EmpatesFora", parseInt(wsChamps["EmpatesFora"]));
    bdChamps.set("DerrotasFora", parseInt(wsChamps["DerrotasFora"]));
    
    bdChamps.set("Aproveitamento", parseFloat(wsChamps["Aproveitamento"]));
    bdChamps.set("PontoMaximo", parseInt(wsChamps["PontoMaximo"]));

    //if (parseInt(wsChamps["IdCampeonato"]) == 476) {
    //    console.log("---> " + wsChamps["NomeCampeonato"] + " - " + parseInt(wsChamps["IdEquipe"]) + " - " + wsChamps["NomeEquipe"] + " - " + wsChamps["Pontos"]);
    //}
    return bdChamps;
};

exports.deleteAll = function(req) {
  return ws.deleteAll(Score);
};
