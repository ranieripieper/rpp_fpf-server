
var Installation = Parse.Object.extend('Installation');

var pushToChannel = function(channels, message, idPartida){
    console.log('Sending a push (pushToChannel): ' + message);
    return Parse.Push.send({
        channels: channels,
        data: {
            alert: message,
            idPartida: idPartida
        }
    });
};

var pushToQuery = function(queryPush, message, idPartida){
    console.log('Sending a push (pushToQuery): ' + message);
    return Parse.Push.send({
    	where: queryPush,
        data: {
            alert: message,
            idPartida: idPartida
        }
    });
};

exports.processPush = function(channels, message, idPartida, idCampeonato){
	console.log('Sending a push: processPush ' + message);
	//se for libertadores só envia para Android >= 1.1.3 e iOS >= 12
    if (idCampeonato == 449) {
        var queryAndroid = new Parse.Query(Installation);
        queryAndroid.equalTo('deviceType', "android");
        queryAndroid.greaterThanOrEqualTo('appVersion', "1.1.3");
        queryAndroid.containedIn('channels', channels);

        var queryIos = new Parse.Query(Installation);
        queryIos.equalTo('deviceType', "ios");
        queryIos.greaterThanOrEqualTo('appVersion', "12");
        queryIos.containedIn('channels', channels);

        var queryPush = Parse.Query.or(queryAndroid, queryIos);
        return pushToQuery(queryPush, message, idPartida);
    } else if (idCampeonato == 475 || idCampeonato == 476 || idCampeonato == 477 || idCampeonato == 464) {
        // se brasileiro Séria A, B, C e Copa do Brasil
        var queryAndroid = new Parse.Query(Installation);
        queryAndroid.equalTo('deviceType', "android");
        queryAndroid.greaterThanOrEqualTo('appVersion', "1.1.4");
        queryAndroid.containedIn('channels', channels);

        var queryIos = new Parse.Query(Installation);
        queryIos.equalTo('deviceType', "ios");
        queryIos.greaterThanOrEqualTo('appVersion', "15");
        queryIos.containedIn('channels', channels);

        var queryPush = Parse.Query.or(queryAndroid, queryIos);
        return pushToQuery(queryPush, message, idPartida);
    } else {
        return pushToChannel(channels, message, idPartida);
    }
};


exports.pushToQuery = pushToQuery;
exports.pushToChannel = pushToChannel;



                            