var _ = require('underscore');

var isTwoEquals = function(obj1, obj2, uniqueProperties){
    
    if(obj1 == null || obj2 == null){
        return false;
    }
    
    for(var i = 0;i < uniqueProperties.length;i++){
        if(obj1.get(uniqueProperties[i]) != obj2.get(uniqueProperties[i])){
            return false;
        }
    }
    
    return true;
};

var findAllTobeRemoved = function(targetClass, uniqueProperties, skip) {
    var allToBeRemoved = [],
        limit = 1000;
    skip = skip || 0;
    console.log("Start finding duplicates " + targetClass.className + " object(s) (skip=" + skip + ")");
    
    if(uniqueProperties == null || uniqueProperties.length == 0)
        limit = 0;
        
    var query = new Parse.Query(targetClass);
    query.descending(uniqueProperties);
    query.addDescending("createdAt");
    query.skip(skip);
    query.limit(1000);
    query.select(uniqueProperties);
        
    return query.find().then(function(results) {
        
        if(results == null){
            console.log("There is not any " + targetClass.className + " object do handle");
            return null;
        } else {
            console.log(results.length + " " + targetClass.className + " object(s) found to remove duplicated entries");
        }
            
        var last = null;
                    
        for(var i = 0;i < results.length;i++){
            var obj = results[i];
            
            if(last != null){
                if(isTwoEquals(last, obj, uniqueProperties)){
                    allToBeRemoved.push(obj);
                    continue;
                }
            }
            
            last = obj;
        }
        
        if(results.length == 1000){
            skip += 1000;
            
            if(skip > 10000){
                return allToBeRemoved;
            }
            
            console.log("Reached max objects per query, trying again with skip (" + skip + ")");
            return findAllTobeRemoved(targetClass, uniqueProperties, skip).then(function(results) {
                return allToBeRemoved.concat(results);
                
            });
        }
        
        return allToBeRemoved;
   });
};

var queryAllFromClassAndRemoveDuplicates = function(targetClass, uniqueProperties){
    return findAllTobeRemoved(targetClass, uniqueProperties, 0).then(function(allToBeRemoved) {
        console.log(allToBeRemoved.length + " " + targetClass.className + " object(s) removed");
        return Parse.Object.destroyAll(allToBeRemoved);
    });
};

exports.queryAllFromClassAndRemoveDuplicates = queryAllFromClassAndRemoveDuplicates;
